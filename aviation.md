---
layout: page
css: "/css/portfolio.css"
title: "Aviation"
subtitle: "Un bel avion est un avion qui vole bien."
active: gallery
header-img: "img/gallery-bg.jpg"
album-title: ""
comments: false
images:
 - image_path: /img/portfolio/g01/bg1.jpg
   caption: Gueule de requin
   mfp-title: Gueule de requi
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg2.jpg
   caption: Décollage au coucher du soleil
   mfp-title: Décollage au coucher du soleil
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg3.jpg
   caption: CL415 & Tracker
   mfp-title: CL415 & Tracker
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg4.jpg
   caption: Beech D18S
   mfp-title: Beech D18S
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg5.jpg
   caption: N3N au décollage
   mfp-title: N3N au décollage
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg6.jpg
   caption: Hangar 7 Red Bull
   mfp-title: Hangar 7 Red Bull
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg7.jpg
   caption: B757 OpenSkies
   mfp-title: B757 OpenSkies
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg8.jpg
   caption: Montgolfière
   mfp-title: Montgolfière
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg9.jpg
   caption: Canadair CL415
   mfp-title: Canadair CL415
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g01/bg10.jpg
   caption: Alphajet PAF
   mfp-title: Alphajet PAF
   copyright: © Daniel E. Photography
---

<html class="no-js" lang="fr">
<head>
	<meta content="charset=utf-8">
</head>

    <body>
 
	<section id="content" role="main">
		<div class="wrapper">
	<br><br>
			<h2>{{page.album-title}}</h2>


			<!-- Gallery __-->
			<div class="gallery masonry-gallery">
		
{% for image in page.images %}  		

				<figure class="gallery-item">
					<header class='gallery-icon'>

<a href="{{ site.url }}{{ site.baseurl }}{{ image.image_path }}" class="popup"  title="{{ image.caption }}" data-caption="{{ image.copyright }}">
<img src="{{ site.url }}{{ site.baseurl }}{{ image.image_path }}"></a>
						
					</header>	
					<figcaption class='gallery-caption'>
						<div class="entry-summary" id="{{ image.caption }}">
							<h3>{{image.caption}}</h3>
							<p>{{image.copyright}}</p>
						</div>
					</figcaption>
				</figure>
				
{% endfor %}

			</div>

		</div><!-- END .wrapper -->
	</section>

<!-- jQuery -->    

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- include image popups -->
<script src="{{ site.baseurl }}/js/jquery.magnific-popup.js"></script>

<script type="text/javascript">
      $(document).ready(function($) {
        $('a.popup').magnificPopup({
         type: 'image',
	  gallery:{
         enabled:true,
         navigateByImgClick: true,
         preload: [0,1] // Will preload 0 - before current, and 1 after the current image
       },
image: {
      titleSrc: function(item) {
              return item.el.attr('title') + '&nbsp;' + item.el.attr('data-caption');
            }
        }
          // other options
      });
});
    </script>

<script src="{{ site.baseurl }}/js/retina.min.js"></script>
<!-- include Masonry -->
<script src="{{ site.baseurl }}/js/isotope.pkgd.min.js"></script> 
<!-- include mousewheel plugins -->
<script src="{{ site.baseurl }}/js/jquery.mousewheel.min.js"></script>
<!-- include carousel plugins -->
<script src="{{ site.baseurl}}/js/jquery.tinycarousel.min.js"></script>
<!-- include svg line drawing plugin -->
<script src="{{ site.baseurl }}/js/jquery.lazylinepainter.min.js"></script>
<!-- include custom script -->
<script src="{{ site.baseurl }}/js/scripts.js"></script>
<!-- Modernizr -->
 <script src="{{ site.baseurl }}/js/modernizr.js"></script>

    
</body></html>
