---
layout: page
css: "/css/index.css"
meta-title: "Daniel E. Photography"
meta-description: "Portfolio et Blog"
bigimg:
  - "/img/big-imgs/home1.jpg" : "Aviation"
  - "/img/big-imgs/home2.jpg" : "Photo de rue"
  - "/img/big-imgs/home3.jpg" : "Portrait"
---

<div class="list-filters">
  <a href="/" class="list-filter filter-selected">Tout les billets</a>
  <a href="/popular" class="list-filter">Billets Populaires</a>
  <a href="/tags" class="list-filter">Tags</a>
</div>

<div class="posts-list">
  {% for post in site.tags.popular %}
  <article>
    <a class="post-preview" href="{{ post.url | prepend: site.baseurl }}">
	    <h2 class="post-title">{{ post.title }}</h2>
	
	    {% if post.subtitle %}
	    <h3 class="post-subtitle">
	      {{ post.subtitle }}
	    </h3>
	    {% endif %}
      <p class="post-meta">
           Publié le {{ post.date | date: "%d/%m/%Y" }}
      </p>

      <div class="post-entry">
        {{ post.content | truncatewords: 50 | strip_html | xml_escape}}
        <span href="{{ post.url | prepend: site.baseurl }}" class="post-read-more">[Lire la suite]</span>
      </div>
    </a>  
   </article>
  {% endfor %}
</div>
