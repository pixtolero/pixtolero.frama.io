---
layout: page
title: "Noir & Blanc"
css: "/css/portfolio.css"
subtitle: "Une photographie, c'est un arrêt du coeur d'une fraction de seconde."
active: gallery
header-img: "img/gallery-bg.jpg"
album-title: ""
images:
 - image_path: /img/portfolio/g02/bg1.jpg
   caption: Pilote Luftwaffe WW2
   mfp-title: Pilote Luftwaffe WW2
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg2.jpg
   caption: Canal Saint-Martin
   mfp-title: Canal Saint-Martin
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg3.jpg
   caption: Le Peintre de Notre-Dame
   mfp-title: Le Peintre de Notre-Dame
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg4.jpg
   caption: A320 en finale
   mfp-title: A320 en finale
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg5.jpg
   caption: P-51 Mustang
   mfp-title: P-51 Mustang
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg6.jpg
   caption: Jaguar
   mfp-title: Jaguar
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg7.jpg
   caption: Red Bull Hangar 7
   mfp-title: Red Bull Hangar 7
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg8.jpg
   caption: Cadillac
   mfp-title: Cadillac
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g02/bg9.jpg
   caption: Montgolfière
   mfp-title: Montgolfière
   copyright: © Daniel E. Photography
---

<html class="no-js" lang="fr">
<head>
	<meta content="charset=utf-8">
</head>

    <body>
 
	<section id="content" role="main">
		<div class="wrapper">
	<br><br>
			<h2>{{page.album-title}}</h2>


			<!-- Gallery __-->
			<div class="gallery masonry-gallery">
		
{% for image in page.images %}  		

				<figure class="gallery-item">
					<header class='gallery-icon'>

<a href="{{ site.url }}{{ site.baseurl }}{{ image.image_path }}" class="popup" title="{{ image.caption }}" data-caption="{{ image.copyright }}">
<img src="{{ site.url }}{{ site.baseurl }}{{ image.image_path }}"></a>
						
					</header>	
					<figcaption class='gallery-caption'>
						<div class="entry-summary">
							<h3>{{image.caption}}</h3>
							<p>{{image.copyright}}</p>
						</div>
					</figcaption>
				</figure>
				
{% endfor %}

			</div>

		</div><!-- END .wrapper -->
	</section>

<!-- jQuery -->    

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- include image popups -->
<script src="{{ site.baseurl }}/js/jquery.magnific-popup.js"></script>
<script src="{{ site.baseurl }}/js/retina.min.js"></script>
<!-- include Masonry -->
<script src="{{ site.baseurl }}/js/isotope.pkgd.min.js"></script> 
<!-- include mousewheel plugins -->
<script src="{{ site.baseurl }}/js/jquery.mousewheel.min.js"></script>
<!-- include carousel plugins -->
<script src="{{ site.baseurl}}/js/jquery.tinycarousel.min.js"></script>
<!-- include svg line drawing plugin -->
<script src="{{ site.baseurl }}/js/jquery.lazylinepainter.min.js"></script>
<!-- include custom script -->
<script src="{{ site.baseurl }}/js/scripts.js"></script>
<!-- Modernizr -->
 <script src="{{ site.baseurl }}/js/modernizr.js"></script>

    <script type="text/javascript">
      $(document).ready(function($) {
        $('a.popup').magnificPopup({
          type: 'image',
	  gallery:{
         enabled:true,
         navigateByImgClick: true,
         preload: [0,1] // Will preload 0 - before current, and 1 after the current image
       },
      image: {
         titleSrc: function(item) {
             return item.el.attr('title') + '&nbsp;' + item.el.attr('data-caption');
            }
        }
          // other options
      });
});
    </script>

</body></html>
