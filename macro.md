---
layout: page
title: "Macro"
css: "/css/portfolio.css"
subtitle: "Si ta photographie n'est pas bonne, c'est que tu n'étais pas assez près."
active: gallery
header-img: "img/gallery-bg.jpg"
album-title: ""
images:
 - image_path: /img/portfolio/g04/bg1.jpg
   caption: Cétoine dorée
   mfp-title: Cétoine dorée
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg2.jpg
   caption: Sur le fil
   mfp-title: Sur le fil
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg3.jpg
   caption: Étoile à bandes
   mfp-title: Étoile à bandes
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg4.jpg
   caption: Mante Religieuse
   mfp-title: Mante Religieuse
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg5.jpg
   caption: Jardin des Plantes
   mfp-title: Jardin des Plantes
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg6.jpg
   caption: Jardin des Plantes
   mfp-title: Jardin des Plantes
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg7.jpg
   caption: Mantes Religieuses
   mfp-title: Mantes Religieuses
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg8.jpg
   caption: Givre
   mfp-title: Givre
   copyright: © Daniel E. Photography
 - image_path: /img/portfolio/g04/bg9.jpg
   caption: Gouttes d'eau
   mfp-title: Gouttes d'eau
   copyright: © Daniel E. Photography
---

<html class="no-js" lang="fr">
<head>
	<meta content="charset=utf-8">
</head>

    <body>
 
	<section id="content" role="main">
		<div class="wrapper">
	<br><br>
			<h2>{{page.album-title}}</h2>


			<!-- Gallery __-->
			<div class="gallery masonry-gallery">
		
{% for image in page.images %}  		

				<figure class="gallery-item">
					<header class='gallery-icon'>

<a href="{{ site.url }}{{ site.baseurl }}{{ image.image_path }}" class="popup" title="{{ image.caption }}" data-caption="{{ image.copyright }}">
<img src="{{ site.url }}{{ site.baseurl }}{{ image.image_path }}"></a>
						
					</header>	
					<figcaption class='gallery-caption'>
						<div class="entry-summary">
							<h3>{{image.caption}}</h3>
							<p>{{image.copyright}}</p>
						</div>
					</figcaption>
				</figure>
				
{% endfor %}

			</div>

		</div><!-- END .wrapper -->
	</section>

<!-- jQuery -->    

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- include image popups -->
<script src="{{ site.baseurl }}/js/jquery.magnific-popup.js"></script>
<script src="{{ site.baseurl }}/js/retina.min.js"></script>
<!-- include Masonry -->
<script src="{{ site.baseurl }}/js/isotope.pkgd.min.js"></script> 
<!-- include mousewheel plugins -->
<script src="{{ site.baseurl }}/js/jquery.mousewheel.min.js"></script>
<!-- include carousel plugins -->
<script src="{{ site.baseurl}}/js/jquery.tinycarousel.min.js"></script>
<!-- include svg line drawing plugin -->
<script src="{{ site.baseurl }}/js/jquery.lazylinepainter.min.js"></script>
<!-- include custom script -->
<script src="{{ site.baseurl }}/js/scripts.js"></script>
<!-- Modernizr -->
 <script src="{{ site.baseurl }}/js/modernizr.js"></script>

    <script type="text/javascript">
      $(document).ready(function($) {
        $('a.popup').magnificPopup({
          type: 'image',
	  gallery:{
         enabled:true,
         navigateByImgClick: true,
         preload: [0,1] // Will preload 0 - before current, and 1 after the current image
       },
      image: {
         titleSrc: function(item) {
              return item.el.attr('title') + '&nbsp;' + item.el.attr('data-caption');
            }
        }
          // other options
      });
});
    </script>

</body></html>
