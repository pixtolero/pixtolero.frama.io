---
layout:     post
title:      "La Ferté, encore et encore"
date:       2018-10-23
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation]
categories: [aviation]
comments: true
---

<p>Voici un medley de mes dernières virées à la Ferté.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/45019652612/in/dateposted-public/" title="Stearman PT-17"><img src="https://farm2.staticflickr.com/1901/45019652612_4615f0254d_z.jpg" width="640" height="426" alt="Stearman PT-17"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/44444780515/in/dateposted-public/" title="Becch D18S"><img src="https://farm2.staticflickr.com/1933/44444780515_37c710b6fd_z.jpg" width="640" height="426" alt="Becch D18S"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/30418461847/in/dateposted-public/" title="T-6 Engine"><img src="https://farm2.staticflickr.com/1943/30418461847_e99d62a11b_z.jpg" width="640" height="426" alt="T-6 Engine"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/43255472370/in/dateposted-public/" title="Stearman PT-17"><img src="https://farm2.staticflickr.com/1933/43255472370_d32c9ae960_z.jpg" width="640" height="426" alt="Stearman PT-17"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/44635069574/in/dateposted-public/" title="Royal Enfield"><img src="https://farm2.staticflickr.com/1946/44635069574_770a546e3a_z.jpg" width="640" height="426" alt="Royal Enfield"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/45308959102/in/dateposted-public/" title="Stearman PT-17"><img src="https://farm2.staticflickr.com/1976/45308959102_9c4ba04e1d_z.jpg" width="640" height="426" alt="Stearman PT-17"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/43542957430/in/dateposted-public/" title="Harley-Davidson"><img src="https://farm2.staticflickr.com/1933/43542957430_a04fc641ef_z.jpg" width="640" height="426" alt="Harley-Davidson"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/31484400008/in/dateposted-public/" title="Caudron GIII"><img src="https://farm2.staticflickr.com/1933/31484400008_afefb0c8a3_z.jpg" width="640" height="426" alt="Caudron GIII"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
