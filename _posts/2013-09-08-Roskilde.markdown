---
layout:     post
title:      "Musée des bateaux vikings de Roskilde"
subtitle:   ""


date:       2013-09-08
author:     "owner_name"

tags: [divers]
categories: [divers,paysage]
comments: true
---

<p>Toujours au Danemark, j'ai visité le Vikingeskibsmeuseet, le musée de bateaux vikings de Roskilde (au nord-ouest de Copenhague). Cinq épaves de bateaux viking avaient été retrouvés dans le fjord de Roskilde et après avoir crée une île artificielle ainsi qu'après de 30 ans de restauration, le musée fut bâti pour les accueillir. Vous verrez sur les photos que le travail de restauration est remarquable. Le musée possède également des répliques avec lesquelles il est possible de faire un tour dans le fjord. Il se trouve que le même jour de ma visite se tenait un festival viking avec des gens déguisés pour l'occasion et exerçant des activités viking (découpe du bois, repas typique...).</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/26594336487/in/album-72157665776877507/" title="Viking Ships Museum (Roskilde)"><img src="https://farm1.staticflickr.com/877/26594336487_4a234dd6d9_c.jpg" width="533" height="800" alt="Viking Ships Museum (Roskilde)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
