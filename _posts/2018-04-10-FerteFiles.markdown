---
layout:     post
title:      "Filés à La Ferté"
date:       2018-04-10
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation]
categories: [aviation]
comments: true
---

<p>Retour à La Ferté samedi dernier. Cette fois, j'ai retiré l'extender pour ne garder que le 200mm F2.8. Malgré la perte de distance de focale, je gagne en piqué et un peu en vélocité de l'AF. Résultats, pour les décollages, j'ai remarqué qu'une vitesse autour de 1/60 marche plutôt pas mal comme plus bas avec le BB jodel (le jaune), le Cessna a été pris à 1/25 mais ca devient juste.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/27466344398/in/album-72157684588283154/" title="Bébé Jodel"><img src="https://farm1.staticflickr.com/892/27466344398_f4633d3d13_z.jpg" width="640" height="426" alt="Bébé Jodel"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/41306355522/in/album-72157684588283154/" title="Cessna C172R"><img src="https://farm1.staticflickr.com/885/41306355522_bfb2e7e1b9_z.jpg" width="640" height="426" alt="Cessna C172R"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/41298639702/in/album-72157684588283154/" title="BJ-301"><img src="https://farm1.staticflickr.com/790/41298639702_320835237e_z.jpg" width="640" height="426" alt="BJ-301"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/39532967500/in/album-72157684588283154/" title="Morane-Sulnier MS.317"><img src="https://farm1.staticflickr.com/786/39532967500_1b1cc2f1de_z.jpg" width="640" height="426" alt="Morane-Sulnier MS.317"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/26468576877/in/album-72157684588283154/" title="North American T-6G Texan"><img src="https://farm1.staticflickr.com/876/26468576877_2cb288353f_z.jpg" width="640" height="426" alt="North American T-6G Texan"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>Le mmeeting se rapprochant quelques avions de collection étaient de la partie et j'ai aussi profité de la visite du musée volant pour me faire un kiffe noir et blanc avec le B-17.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/40447480695/in/album-72157684588283154/" title="B-17 Flying Fortress"><img src="https://farm1.staticflickr.com/879/40447480695_08936a71ff_z.jpg" width="640" height="404" alt="B-17 Flying Fortress"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/26474232677/in/album-72157684588283154/" title="Beech D18S"><img src="https://farm1.staticflickr.com/802/26474232677_3c5d081e03_z.jpg" width="640" height="426" alt="Beech D18S"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/39537941330/in/album-72157684588283154/" title="Bücker Bü-133 Jungmeister"><img src="https://farm1.staticflickr.com/818/39537941330_4e0192a17f_z.jpg" width="640" height="426" alt="Bücker Bü-133 Jungmeister"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/41297504172/in/album-72157684588283154/" title="North American T-6G Texan"><img src="https://farm1.staticflickr.com/819/41297504172_34afae6794_z.jpg" width="640" height="426" alt="North American T-6G Texan"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/41299600452/in/album-72157684588283154/" title="Morane-Sulnier MS.317"><img src="https://farm1.staticflickr.com/797/41299600452_96686da85e_z.jpg" width="640" height="426" alt="Morane-Sulnier MS.317"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>La photo suivante représente le BB Jodel au sol avec Jack Krine aux commandes. Rien d'exceptionnel, pourtant un membre de Flickr a invité la photo au groupe "In explore" et j'ai accepté. Depuis, pas mal de monde a mis cette photo en favoris, je récupère un peu plus de visibilité sur Flickr même si j'aurais préféré que ce soit une photo plus intéressante comme celle avec le même avion en rase motte avec le filé...enfin tanpis je prend quand même.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/41338379441/in/album-72157684588283154/" title="Bébé Jodel"><img src="https://farm1.staticflickr.com/880/41338379441_eb6fbd2208_z.jpg" width="640" height="426" alt="Bébé Jodel"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>








