---
layout:     post
title:      "Et un objectif Macro !"
subtitle:   ""


date:       2012-11-26
author:     "owner_name"
header-img: "img/postcover/divers.jpg"
tags: [nouvelles]
categories: [nouvelles]
comments: true
---

<p>Je vais pouvoir enfin m’attaquer à la macrophotographie. La motorisation USM est une surprenante découverte pour moi, fini les bruits de l’autofocus.</p>

<a href="http://2.bp.blogspot.com/-axoiNq3biSY/ULOlkJXTcNI/AAAAAAAAQNA/Sqd3yhZpTVs/s1600/IMG_20121126_172425.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-axoiNq3biSY/ULOlkJXTcNI/AAAAAAAAQNA/Sqd3yhZpTVs/s640/IMG_20121126_172425.jpg" /></a>
