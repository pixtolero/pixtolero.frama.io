---
layout:     post
title:      "Fly In Stearman 2014"
subtitle:   ""


date:       2014-08-26
author:     "owner_name"

tags: [aviation]
categories: [aviation]
comments: true
---

<p>Cette année, les propriétaires de Stearman PT-17 étaient conviés à un rassemblement sur le terrain d'aviation de La Ferté-Alais. Etant à distance des avions, je m'y suis rendu équipé du 200mm avec le converter 1.4x. Le temps était gris avec parfois des éclaircies. Le TravelAir, le T-6, un ULM et un Zéro (T-6 déguisé) étaient de la partie pour des baptêmes de l'air. Je suis assez content du résultat et ce 200mm continue à m'impressionner.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/39796834720/in/album-72157695199911734/" title="Stearman PT-17"><img src="https://farm1.staticflickr.com/880/39796834720_798a7436f1_c.jpg" width="800" height="533" alt="Stearman PT-17"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
