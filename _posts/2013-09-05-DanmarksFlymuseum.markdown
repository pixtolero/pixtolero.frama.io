---
layout:     post
title:      "Danmarks Flymuseum"
subtitle:   ""


date:       2013-09-05
author:     "owner_name"

tags: [aviation]
categories: [aviation]
comments: true
---

<p>Pour aller au Danmarks Flymuseum depuis Copenhague, il faut traverser le pays en large. En bref il faut franchir le pont qui relie l'île où se trouve Copenhague pour arriver sur une autre île, puis franchir un autre pont qui sépare cette dernière île du continent. Une fois quasiment à l'autre bout du pays, non loin de la ville de Skjern et plus précisément sur l'aéroport de Stauning, on découvre enfin le plus riche musée d’aviation du Danemark. Certes, ce musée n'est pas aussi vaste que celui du Bourget mais sa collection est assez exceptionnelle. Il est divisé en 3 parties. La première salle pour les débuts de l'aviation, l'aviation légère et le transport civil. La seconde partie est consacrée à la seconde guerre mondiale et aux avions "KZ" de fabrication danoise. Enfin la troisième salle retrace l'histoire et expose des machines de la force aérienne royale danoise. D'après ce que j'ai compris Staunig est une sorte de berceau de l'aviation au Danemark, ce qui explique pourquoi le musée est situé là bas et pas proche de la capitale. À noter que certains des avions sont encore en état de vol.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41412544192/in/album-72157695646445505/" title="Danmarks Flymuseum"><img src="https://farm1.staticflickr.com/806/41412544192_9932da574c_c.jpg" width="800" height="533" alt="Danmarks Flymuseum"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
