---
layout:     post
title:      "Zoo de Copenhague"
subtitle:   ""

header-img: "img/postcover/animalier.jpg"
date:       2013-09-09
author:     "owner_name"

tags: [copenhague,animalier]
categories: [animalier]
comments: true
---

<p>J’ai terminé mon séjour au Danemark par le Zoo de Copenhague. Il y a un grand nombre d’espèces d’animaux et certains enclos sont impressionnants, en particulier celui des éléphants. L’atout du Zoo est surement le couple d’ours blancs mais qui étaient difficiles à photographier car soit ils étaient trop éloignés, soit ils s’amusaient dans l’eau. Néanmoins ce fût un bel après-midi pour la photo animalière, je suis assez fier des portraits d’ours brun ou de lézard.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/9712079680/in/album-72157635450699729/" title="2013-08-25_14-57-2430CopenhagenZoo"><img src="https://farm3.staticflickr.com/2884/9712079680_ea7b43cbbb_c.jpg" width="800" height="533" alt="2013-08-25_14-57-2430CopenhagenZoo"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
