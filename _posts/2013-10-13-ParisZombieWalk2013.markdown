---
layout:     post
title:      "Paris Zombie Walk 2013"
subtitle:   ""


date:       2013-10-13
author:     "owner_name"

tags: [zombie,portrait,cosplay,paris]
categories: [portrait]
comments: true
---

<p>Hier avait lieu la Paris Zombie Walk sur la Place de la République. Vers les 13h, la place s’est rapidement remplie de divers groupes de zombies. Les déguisements étaient franchement réussis. Chapeau aux zombies des personnages de la série TV Breaking Bad, Walter White et Jesse Pinkman dans leur combinaisons jaunes étaient particulièrement effrayants. Une bonne ambiance régnait sur place, il y avait même un stand de maquillage. Le docteur de Doctor Who était de passage avec l’une des ses accompagnatrices. Sur un coté de la place, il y avait un groupe qui dansait sur  de Michael Jackson. Un couple de mariés zombie a fait une scène de ménage, cette mise en scène était assez drôle. Même si je n’ai pas suivi le cortège, ce fut un bel événement. Je trouve certaines photos assez réussies avec l’aide de certains presets Lightroom qui leur donne un coté apocalyptique qui colle parfaitement au thème zombie. Voici une série de quelques clichés. Voici une sélection, le reste des photos se trouve <a href="https://www.flickr.com/photos/danpilote/sets/72157667762680008">sur Flickr</a>:</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/40720271774/in/album-72157667762680008/" title="Paris Zombie Walk 2013"><img src="https://farm1.staticflickr.com/797/40720271774_f1dab67935_c.jpg" width="800" height="533" alt="Paris Zombie Walk 2013"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
