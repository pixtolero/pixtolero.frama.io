---
layout:     post
title:      "Rétrospective 2017"
subtitle:   ""

header-img: "img/postcover/divers.jpg"
date:       2017-12-29
author:     "owner_name"

tags: [nouvelles,aviation,divers,portrait,voiture,cosplay,paysage]
categories: [nouvelles,divers,aviation,portrait,voiture]
comments: true
---

<p>Ce ne fût pas une année très fructueuse en photo mais j'ai quand même pu faire quelques trucs intéressants.</p>

<p>Coté Aviation et spotting, j'ai juste fait quelques escapades à la Ferté et en dehors du meeting annuel. Cela m'a permis de faire quelques clichés sympas et m'exercer au filé des avions au décollage sans stress.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38042673951/in/album-72157684588283154/" title="North American T-6 G (F-HLEA)"><img src="https://farm5.staticflickr.com/4466/38042673951_0266a1142c_z.jpg" width="640" height="426" alt="North American T-6 G (F-HLEA)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36232508352/in/album-72157684588283154/" title="Boeing Stearman Kaydet PT13D / N2S5"><img src="https://farm5.staticflickr.com/4433/36232508352_b7228fb97f_z.jpg" width="640" height="426" alt="Boeing Stearman Kaydet PT13D / N2S5"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36003660550/in/album-72157684588283154/" title="North American T-6G (F-ABZQ)"><img src="https://farm5.staticflickr.com/4409/36003660550_49e85148dd_z.jpg" width="640" height="426" alt="North American T-6G (F-ABZQ)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38028575352/in/album-72157684588283154/" title="Boeing E75N1 Stearman (N43SV)"><img src="https://farm5.staticflickr.com/4479/38028575352_2ebe6de158_z.jpg" width="640" height="426" alt="Boeing E75N1 Stearman (N43SV)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>En septembre, j'ai visité l'expo des super héros DC et j'ai opté pour smartphone comme appareil photo, il s'en est pas si mal sorti le bougre.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36119744950/in/album-72157684978937663/" title="Tumbler (Dark Knight Trilogy)"><img src="https://farm5.staticflickr.com/4406/36119744950_16ce14a4b0_z.jpg" width="640" height="360" alt="Tumbler (Dark Knight Trilogy)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36378770681/in/album-72157684978937663/" title="Statue Batman (The Dark Knight rises)"><img src="https://farm5.staticflickr.com/4350/36378770681_bb9195bd3c_z.jpg" width="360" height="640" alt="Statue Batman (The Dark Knight rises)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>J'ai tenté du portrait au festival harajuku mais sans véritable succès. J'avais juste fait que 3 photos. Je ne fût guère plus inspiré lors de Paris Manga.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/37108283286/in/album-72157689220293895/" title="Poison Ivy"><img src="https://farm5.staticflickr.com/4437/37108283286_957fbe7fa6_z.jpg" width="368" height="640" alt="Poison Ivy"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>Coté matos, je me suis offert un Canon EF-S 10-18mm IS STM d'occasion qui a été assez performant lors de prise de vue de la BnF. Ce fût un bon achat et j'espère avoir l'occasion de le réutiliser l'année prochaine.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/37113586826/in/dateposted-public/" title="BnF"><img src="https://farm5.staticflickr.com/4379/37113586826_5920e0e42c_z.jpg" width="640" height="407" alt="BnF"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36922794010/in/dateposted-public/" title="BnF"><img src="https://farm5.staticflickr.com/4429/36922794010_9d9afc6d45_z.jpg" width="426" height="640" alt="BnF"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p> J'ai terminé cette année photos avec le Salon du Chocolat pour des plans rapprochés avec le 100mm Macro.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/37360836804/in/album-72157688650807654/" title="Salon du Chocolat 2017"><img src="https://farm5.staticflickr.com/4475/37360836804_1ddf7fcf29_z.jpg" width="640" height="426" alt="Salon du Chocolat 2017"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/37360737964/in/album-72157688650807654/" title="Salon du Chocolat 2017"><img src="https://farm5.staticflickr.com/4504/37360737964_283e8eb9fc_z.jpg" width="426" height="640" alt="Salon du Chocolat 2017"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38070528721/in/album-72157688650807654/" title="Salon du Chocolat 2017"><img src="https://farm5.staticflickr.com/4443/38070528721_f4f6c2a7ab_z.jpg" width="640" height="426" alt="Salon du Chocolat 2017"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>Je pensais changer de boitier mais vu mon peu d'activité en photo cette année je préfère user mon EOS 550D au max.</p>

<p>La nouvelle version stable de darktable, la 2.4.0, est sortie depuis Noel. Le site francophone que j'administre subit une hausse des visites car une version windows est maintenant disponible et j'en suis vraiment content. Je profites également d'un peu de temps libre pour retraiter avec darktable d'anciennes photos de l'époque où j'utilisais Lightroom.</p>

<p>Pour 2018, pas de promesses mais j'espère quand même faire davantage de photos.</p>
