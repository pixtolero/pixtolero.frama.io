---
layout:     post
title:      "Encore un peu de la Ferté"
subtitle:   ""


date:       2017-09-28
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation]
categories: [aviation]
comments: true
---

<p>Un petit best of de mes dernières photos à la ferté.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38028575352/in/album-72157684588283154/" title="Boeing E75N1 Stearman (N43SV)"><img src="https://farm5.staticflickr.com/4479/38028575352_2ebe6de158_z.jpg" width="640" height="426" alt="Boeing E75N1 Stearman (N43SV)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38042673951/in/album-72157684588283154/" title="North American T-6 G (F-HLEA)"><img src="https://farm5.staticflickr.com/4466/38042673951_0266a1142c_z.jpg" width="640" height="426" alt="North American T-6 G (F-HLEA)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38042701131/in/album-72157684588283154/" title="North American T-6 G (F-HLEA)"><img src="https://farm5.staticflickr.com/4496/38042701131_fbd376ecaa_z.jpg" width="640" height="426" alt="North American T-6 G (F-HLEA)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38065008161/in/album-72157684588283154/" title="YAK 52 -  F-WRUZ"><img src="https://farm5.staticflickr.com/4502/38065008161_3fdf44634b_z.jpg" width="640" height="426" alt="YAK 52 -  F-WRUZ"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38011663862/in/album-72157684588283154/" title="North American T-6 G (F-HLEA)"><img src="https://farm5.staticflickr.com/4510/38011663862_fc418a774a_z.jpg" width="640" height="426" alt="North American T-6 G (F-HLEA)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/38028589232/in/album-72157684588283154/" title="Boeing Stearman Kaydet PT13D / N2S5"><img src="https://farm5.staticflickr.com/4489/38028589232_311152834c_z.jpg" width="640" height="426" alt="Boeing Stearman Kaydet PT13D / N2S5"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


