---
layout:     post
title:      "Retour à La Ferté"
subtitle:   ""


date:       2017-08-09
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation]
categories: [aviation]
comments: true
---


<p>On peut pas dire que cela aura été une année très "photos". Néanmoins, je tente de m'y remettre. Voici quelques photos prises un weekend à la Ferté. L'activité des baptêmes de l'air aide beaucoup ainsi que la présence du nouvel avion d'Aéro Vintage Academy, le T-28.</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36232508352/in/dateposted-public/" title="Boeing Stearman Kaydet PT13D / N2S5"><img src="https://farm5.staticflickr.com/4433/36232508352_b7228fb97f_c.jpg" width="800" height="533" alt="Boeing Stearman Kaydet PT13D / N2S5"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36003681230/in/dateposted-public/" title="North American T-28 (N14113)"><img src="https://farm5.staticflickr.com/4332/36003681230_0690190326_c.jpg" width="800" height="533" alt="North American T-28 (N14113)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36400321235/in/dateposted-public/" title="North American AT-6B-M0 Zero"><img src="https://farm5.staticflickr.com/4408/36400321235_5504f50b89_c.jpg" width="800" height="533" alt="North American AT-6B-M0 Zero"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/36003660550/in/dateposted-public/" title="North American T-6G (F-ABZQ)"><img src="https://farm5.staticflickr.com/4409/36003660550_49e85148dd_c.jpg" width="800" height="533" alt="North American T-6G (F-ABZQ)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/35592447723/in/dateposted-public/" title="Boeing E75N1 Stearman (N43SV)"><img src="https://farm5.staticflickr.com/4384/35592447723_47129b2b45_c.jpg" width="800" height="533" alt="Boeing E75N1 Stearman (N43SV)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/35592494683/in/dateposted-public/" title="Boeing Stearman Kaydet PT13D / N2S5"><img src="https://farm5.staticflickr.com/4336/35592494683_77d0a42cca_c.jpg" width="800" height="533" alt="Boeing Stearman Kaydet PT13D / N2S5"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/35592301463/in/dateposted-public/" title="North American T-28 (N14113)"><img src="https://farm5.staticflickr.com/4396/35592301463_d4f7906229_c.jpg" width="800" height="533" alt="North American T-28 (N14113)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


<p>J'essaierai d'en faire plus régulièrement. J'ai aussi envie de changer de boîtier, le 80d le tente bien, le tout est d'en trouver un d'occasion. Stay tuned !</p>




