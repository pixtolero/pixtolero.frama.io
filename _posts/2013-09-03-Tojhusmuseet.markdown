---
layout:     post
title:      "Visite du Tojhusmuseet"
subtitle:   ""


date:       2013-09-03
author:     "owner_name"

tags: [copenhague]
categories: [divers]
comments: true
---

<p>Le Tojhusmuseet est le Royal Arsenal Museum de Copenhague. Il est situé non loin de Christianborg, là ou siège la famille royale et le gouvernement. J’y suis allé d’abord par simple curiosité et j’ai été impressionné par les collections présentes. Le musée est établie sur deux niveaux. Le premier niveau expose une collection importante de canons en tout genre. Il y avait également une exposition temporaire sur la participation de l’armée danoise en Afghanistan que j’ai trouvé très bien réalisée. Dès le début on marche dans du sable et du gravier, on entend des bruits de mitraillettes, on se croit vraiment dans un camp de la coalition près de Kaboul. Les lieux de repos des soldats, les posters de magazine Playboy, les tubes pour décharger les armes en toute sécurité. À un moment on peut entrer dans une sorte de hutte, on entend des afghans parlés puis les aboiements de chien des soldats danois. Plus loin, après un ruisseau artificiel on découvre une jeep accidenté. Bref une exposition remarquable. Au second niveau, on trouve le reste des collections principalement des armes et des uniformes exposés dans des vitrines ce qui n’a pas rendu la tâche facile pour prendre des photos. Avec un traitement sous Lightroom, j’ai réussi à mettre en valeur certains objets dans les vitrines. En résumé, ce musée fut une intéressante découverte.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41451088461/in/album-72157694911415494/" title="Royal Arsenal Museum (Copenhagen)"><img src="https://farm1.staticflickr.com/817/41451088461_11b6fe415e_c.jpg" width="800" height="533" alt="Royal Arsenal Museum (Copenhagen)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
