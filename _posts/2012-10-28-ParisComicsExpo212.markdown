---
layout:     post
title:      "Paris Comics Expo 2012"
subtitle:   ""


date:       2012-10-28
author:     "owner_name"
header-img: "img/postcover/cosplay.jpg"
tags: [portrait,cosplay]
categories: [portrait,cosplay]
comments: true
---

<p>Première édition de cette expo qui m’a permis de m’essayer à la photographie de cosplay.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/8242963721/in/album-72157632168674108/" title="Black Cat"><img src="https://farm9.staticflickr.com/8350/8242963721_050ded783b_c.jpg" width="800" height="534" alt="Black Cat"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
