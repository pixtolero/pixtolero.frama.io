---
layout:     post
title:      "Deutsches Museum Flugwerft Schleissheim"
subtitle:   ""


date:       2015-08-27
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation,munich]
categories: [aviation]
comments: true
---



<p>Le Deutsches Museum de Munich possède un lieu totalement dédié à l'aéronautique situé à l'aérodrome d'Oberschleissheim, c'est le Deutsches Museum Flugwerft Schleissheim. Le musée est divisé en plusieurs halls et un petit espace pour une exposition temporaire, lors de mon passage, l'expo concernait les prisonniers de guerre.)</p>

<p>Tout d'abord, on débute dans le Hall historique où l'on retrouve divers avions de pionniers, des années 30, d'avions de tourisme et des planeurs. D'ailleurs on retrouve des planeurs en peu partout, l'Allemagne étant une grande nation de ce type d'aéronef (Otto Lilienthal). Avant d’accéder au hall suivant, il y a une partie avec divers objets et matériel avec même un station météo.</p>

<p>Le Hall suivant est une sorte de transition avant le grand Hall d'exposition, c'est un Hall dédié à la maintenance. En fait c'est le hangar de restauration que l'on visite en hauteur, ainsi on peut voir les gens de musée à l'oeuvre sans vraiment les déranger. Pas bête comme idée, pendant ma visite, un MiG-23 était entre autre présent dans l'atelier</p>

<p>Enfin on accède au vaste hall d'exposition où on trouve quelques petites merveilles. Par exemple un Heinkel 111 (ou plutôt sa version espagnol CASA 2.111B), le X-31 à poussée vectorielle, MiG-15, forcement les chasseurs à réaction européen Tornado et Eurofighter, le F-104 Starfighter, un magnifique amphibie Dornier 24 ou encore le Dornier 31 à décollage vertical. D'ailleurs les allemands ont réalisés beaucoup de recherches dans le domaine du décollage vertical, ce qui explique la présence d'appareils dit VTOL. Pas aussi vaste que le Bourget mais ça vaut le détour pour certains appareils de la collection.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41334163405/in/album-72157669183824628/" title="Deutsches Museum Flugwerft Schleissheim"><img src="https://farm1.staticflickr.com/971/41334163405_4f02471a31_c.jpg" width="800" height="533" alt="Deutsches Museum Flugwerft Schleissheim"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
