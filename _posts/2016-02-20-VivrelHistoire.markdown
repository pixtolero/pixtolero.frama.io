---
layout:     post
title:      "Vivre l'Histoire 2016"
subtitle:   ""
image:

date:       2016-02-20
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation,divers,portrait,voiture]
categories: [aviation,divers,portrait,voiture]
comments: true
---

<h1>Vivre l'Histoire 2016</h1>

<p>C'est sur une brochure du Musée des Blindées de Saumur que j'ai prise à Rétromobile que j'ai appris que se tenait un Salon Vivre l'Histoire à l'Espace Champerret. La première édition de ce salon a donc eu lieu le weekend dernier et je n'ai pas hésité très longtemps pour m'y rendre.</p>

<p>Même si le salon occupait qu'une partie de l'Espace Champerret, il y avait des choses à la fois intéressantes et sympathiques. Saumur était donc présent avec un char Renault FT17 de la Première Guerre Mondiale et coté véhicules, on avait également une ambulance Dodge, une Citroën Kegresse (half-track), des Jeeps... J'ai même retrouvé le cockpit du MS406 vu à Rétromobile uen semaine avant. L'ambiance était géniale grâce aux participants déguisés en costumes d'époques (Gladiateurs romains, costumes du Moyen-age, militaires 14-18 ou 39-45...) et aux animations (danses d'époque, simulation de blessures de guerre pour les films, stand Wargaming...). Un concours international de maquettes et figurines s'y tenait et vous verrez sur les photos qu'il y avait du lourd !</p>

<p>Bref, j'ai passé un très bon moment, j'espère que cet événement va prendre de l'ampleur car je compte bien y retourner l'an prochain !</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41660133744/in/album-72157695594627901/" title="Vivre l&#x27;Histoire 2016"><img src="https://farm2.staticflickr.com/1734/41660133744_7588323486_c.jpg" width="800" height="533" alt="Vivre l&#x27;Histoire 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
