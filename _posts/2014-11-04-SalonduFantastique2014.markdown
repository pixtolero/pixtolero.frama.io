---
layout:     post
title:      "Salon du Fantastique 2014"
subtitle:   ""


date:       2014-11-04 
author:     "owner_name"
header-img: "img/postcover/cosplay.jpg"
tags: [cosplay,portrait]
categories: [cosplay,portrait]
comments: true
---

<p>Le weekend dernier j'ai été au Salon du Fantastique à l'espace Champerret, c'était la deuxième année que je m'y rendais. Ce salon est le rendez-vous des fans de jeux de rôle, de médiéval, de steampunk, de littérature Fantasy.. et j'ai eu l'impression qu'il attire plus de monde que la précédente édition. Bravo aux organisateurs car j'ai vraiment apprécié l'ambiance et la bonne humeur présentes sur ce salon. J'ai surtout assisté au spectacle de danse et concours de costume pour faire des photos de portrait. L'espace Champerret n'est pour moi pas vraiment un lieu idéal pour la photo, l'éclairage n'est pas top et j'ai été contraint d'utiliser mon 85mm à grande ouverture (1.8) sachant que l'an passé j'avais opté pour le 100mm 2.8. J'ai mis les ISO en AUTO avec un max de 1600, mon canon 550D gère pas trop mal le bruit jusqu'à cette valeur. L'avantage de l'objectif lumineux me permettait d'avoir une profondeur de champs réduite et donc d'avoir un fond flou, chose nécessaire dans ce genre de situation. Au final, voici une petite sélection (comme d'hab le reste est <a href="https://www.flickr.com/photos/danpilote/albums/72157646796832544">sur Flickr</a>):.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41356560764/in/album-72157695025028501/" title="Salon du Fantastique 2014"><img src="https://farm1.staticflickr.com/963/41356560764_bb52be2f3c_c.jpg" width="800" height="533" alt="Salon du Fantastique 2014"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
