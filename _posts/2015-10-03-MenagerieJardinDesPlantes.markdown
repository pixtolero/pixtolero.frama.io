---
layout:     post
title:      "La Ménagerie du Jardin des Plantes"
subtitle:   ""

header-img: "img/postcover/animalier.jpg"
date:       2015-10-03
author:     "owner_name"

tags: [animalier]
categories: [animalier]
comments: true
---



<p>Voici quelques clichés d'une visite à la Ménagerie du Jardin des Plantes. J'étais muni du 100mm Macro qui est l'objectif idéal pour les zoos, d'une part c'est un téléobjectif et c'est indispensable car dans ce genre d'endroits on se retrouve assez éloigné des animaux, d'autre part, un objectif Macro ça pique et c'est justement intéressant d'avoir un maximum de détails lorsque l'on fait un portrait d'un animal.</p>

<p>La Ménagerie n'est pas aussi vaste qu'un zoo mais le lieu est agréable et présente des espèces intéressantes d'animaux. La plupart des enclos ont un grillage qui empêche de faire de bonne photos malheureusement. La fauverie et le vivarium utilisent du vitrage de protection pas trop sale et permet quand même de faire des photos sympas. J'y était surtout allé pour voir le Panda roux, mais ce dernier était paresseux cet après-midi là..</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41550751794/in/album-72157667208307587/" title="Ménagerie du Jardin des Plantes"><img src="https://farm1.staticflickr.com/955/41550751794_da072912e2_c.jpg" width="800" height="533" alt="Ménagerie du Jardin des Plantes"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
