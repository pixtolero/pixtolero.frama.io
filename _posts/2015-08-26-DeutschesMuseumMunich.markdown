---
layout:     post
title:      "Deutsches Museum de Munich"
subtitle:   ""


date:       2015-08-26
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation,munich]
categories: [aviation]
comments: true
---

<p>Après Salzbourg, je suis resté une journée à Munich en Allemagne, le temps de visiter trois musées d'avions. Le premier de ces derniers est le fameux Deutsches Museum situé au cœur de la ville. Ce musée traite de tout ce qui touche à la technologie et à la science. J'ai seulement pris en photos la partie sur l'aéronautique et ne vous fiez pas aux apparences les autres secteurs sont tout aussi intéressants. Ce musée est tout simplement énorme, j'ai été obligé de le faire en vitesse accélérée et j'ai raté pas mal de choses. Il y a une partie sur les expériences scientifiques où il est possible via des petites attractions de tester différents phénomènes (électricité, réfraction de la lumière..) j'ai rarement vu un musée avec un coté aussi pédagogique. Si vous avez l'occasion d'y aller je vous conseille le sous-sol, car il y a une reconstitution incroyable de mines de charbon, le parcours est tellement long que je demandais quand j'allais en ressortir. On débute avec les premières mines pour finir avec les mines modernes avec véhicules et foreuses, tout simplement impressionnant.</p>

<p>Muni du flash et du 17-40 je me suis, bien sûr, attardé sur la partie aéronautique présente sur deux niveaux. Comme vous le verrez plus bas, la collection est constituée d'avions allemand mythiques tel qu'un Messerchmitt 262 avec ses 2 moteurs à réaction Jumo, un Messerchmitt 163 Komet qui après son autonomie de 10 minutes devenait un véritable planeur, un Junkers 52 dans lequel il est possible de monter, un Junkers F13, l'étonnant avion fusée Bachem Natter,  un Sikorsky avec une moitié de nez vitrée, un Fokker triplan du Baron Rouge ou encore un VJ-101 à décollage vertical. Comme vous pouvez le voir sur les photos, ce musée est très visité et ce n'est pas pour rien !</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/42232677741/in/album-72157696413864524/" title="Deutsches Museum"><img src="https://farm1.staticflickr.com/906/42232677741_99c5939e5f_c.jpg" width="800" height="533" alt="Deutsches Museum"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
