---
layout:     post
title:      "Visite de l’Orlogsmuseet"
subtitle:   ""


date:       2013-09-01
author:     "owner_name"

tags: [copenhague]
categories: [divers]
comments: true
---

<p>L'<a href="http://www.orlogsmuseet.dk/">Orlogsmuseet</a> est le nom danois du musée royal de la marine danoise, situé à Christianshavn, que j'ai eu l'occasion de visiter. Dans ce musée on apprend que le Danemark est une grande nation de marins et que le pays s'est battu sur les mers face aux suédois puis les anglais et enfin les allemands. Le rez de chaussée est constitué d'une salle pour les repas décoré avec des canons et d'une autre pièce de divertissement pour les enfants. Au 1er étage, on découvre une multitude de maquettes et de mise en scène de batailles navales en miniature. L'éclairage, en plus des vitres, n'était pas top pour les photos. Au second étage, on arrive à une marine plus contemporaine avec deux attractions vraiment sympathiques. La première est la visite d'un poste de pilotage d'une sorte de corvette, la seconde est la reconstitution de l'intérieur d'un sous-marin (j'avais parfois du mal à me déplacer avec le sac à dos). Un musée qui mérite le détour si vous passez à Copenhague.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/40739410554/in/album-72157694916768614/" title="Royal Naval Museum (Copenhagen)"><img src="https://farm1.staticflickr.com/792/40739410554_f99a24d3fa_c.jpg" width="800" height="533" alt="Royal Naval Museum (Copenhagen)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
