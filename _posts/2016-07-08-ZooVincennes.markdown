---
layout:     post
title:      "Zoo de Vincennes"
subtitle:   ""

header-img: "img/postcover/animalier.jpg"
date:       2016-07-08
author:     "owner_name"

tags: [animalier]
categories: [animalier]
comments: true
---



<p>Je n'avais pas fait d'animalier depuis la ménagerie du jardin des plantes, je me suis donc décidé à me rendre au zoo de Vincennes, fraîchement rénové.</p>

<p>Les installations et enclos sont remarquables, j'ai particulièrement adoré la volière.</p>

<p>Voici quelques photos faites au 100mm macro, dans un Zoo ce n'est jamais évident, éloignement par rapport aux animaux, animaux non visibles, obstacles comme les vitres ou les grillages...etc </p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/27515924857/in/album-72157667420511257/" title="Zoo de Vincennes"><img src="https://farm2.staticflickr.com/1722/27515924857_a7f7caa633_c.jpg" width="800" height="533" alt="Zoo de Vincennes"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
