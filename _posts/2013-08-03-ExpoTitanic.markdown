---
layout:     post
title:      "Expo Titanic"
subtitle:   ""


date:       2013-08-03
author:     "owner_name"

tags: [expo,paris]
categories: [divers]
comments: true
---

<p>Je me suis rendu cette semaine à l’exposition sur le Titanic au parc des expositions (Porte de Versailles). J’ai réalisé toutes les photos avec le 17-40 F4L qui n’est pas lumineux et l’interdiction du flash m’a contraint à monter en ISO, cela dit, je pense avoir pris quelques clichés sympathiques. À l’entrée de l’exposition se trouvait une grande maquette du paquebot. L’exposition en elle-même était organisée en différentes salles où étaient présents divers objets récupérés. Entre les salles, il y avait des décors reconstitués comme un couloir avec les portes des cabines. La dernière salle était consacrée au naufrage et à l’exploration sous-marine pour retrouver l’épave. Dans cette salle, on retrouve une nouvelle maquette mais cette fois-ci de l’épave enfouie au fond de l’océan. Ce fût une très belle et intéressante exposition.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/26691328687/in/album-72157694039581421/" title="Expo Titanic"><img src="https://farm1.staticflickr.com/861/26691328687_846f2aa088_c.jpg" width="800" height="533" alt="Expo Titanic"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
