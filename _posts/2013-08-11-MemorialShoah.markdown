---
layout:     post
title:      "Le Mémorial de la Shoah"
subtitle:   ""


date:       2013-08-11
author:     "owner_name"

tags: [paris]
categories: [divers]
comments: true
---

<p>À Paris, j'en ai profité pour faire un tour dans <a href="http://www.lieux-insolites.fr/paris/shoah/shoah.htm">un lieu que l'on pourrait trouver insolite</a>, il s'agit du Mémorial de la Shoah. Le mémorial est situé non loin de l'église Saint-Gervais dont je ferais un billet bientôt. L'accès est libre et gratuit mais très sécurisé avec sas et appareil de détection comme dans les aéroports, surement pour parer à un éventuel risque d'attentat. Cet endroit est très riche, on y trouve le mur avec tout les noms des déportés (à l'extérieur de l'enceinte l'un des mur est même dédié aux Justes), le parvis avec son cylindre, la crypte ou encore le mémorial des enfants. J'ai fait quelques photos des endroits qui m'ont semblés les plus marquants. Dans les étages inférieurs, il y a plusieurs salles avec divers documents et objets sur la tragédie et agrémentées de vidéos de témoignages. On peut apprendre énormément de choses, je pense que c'est un lieu à visiter, ne serait-ce que pour le devoir de mémoire.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/9486134472/in/album-72157635018393843/" title="2013-08-08_15-13-1148MemorialShoah"><img src="https://farm6.staticflickr.com/5444/9486134472_f6d4141182_c.jpg" width="800" height="533" alt="2013-08-08_15-13-1148MemorialShoah"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
