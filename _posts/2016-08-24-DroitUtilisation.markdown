---
layout:     post
title:      "Droit d'utilisation"
subtitle:   ""


date:       2016-08-24
author:     "owner_name"
header-img: "img/postcover/divers.jpg"
tags: [nouvelles]
categories: [nouvelles]
comments: true
---

<p>N'étant pas photographe professionnel, la question des droits d'utilisation à des fins commerciales de mes photos a toujours été un peu complexe. Dois-je donner ma permission ? Dois-je demander une contrepartie financière ?... </p>

<p>Il y a peu de temps, un éditeur de Londres m'a contacté via Flickr pour pouvoir utiliser deux de mes photos pour un prochain livre sur le Ju52. Sur le coup, j'étais content, ça fait toujours un peu de pub je me suis dit.</p>

<h2>Utilisation gratuite ? Bah...Pas d'accord.</h2>

<p>En parallèle, je me suis quand même renseigné via quelques recherches sur Google et en demandant sur <a href="https://darktable-fr.tuxfamily.org/forums/sujet/demande-dutilisation-gratuite-dune-photo-pour-un-livre/">le forum de darktable FR</a>.</p>

<p>Les photographes professionnels ont tendance à dire que toutes utilisation  d'une œuvre doit se faire en échange d'une contrepartie. Après tout, c'est parfaitement normal.</p>

<h2>Oui mais au moins ça peut faire un peu de pub ? Bah, en fait pas vraiment.</h2>

<p>La notoriété dans tout ça ?  Peut on faire l'impasse, ne serait-ce que pour un peu de pub ? En feuilletant mes livres d'histoires d'aviation je me suis rendu compte que la plupart des photos proviennent de collections de musée, de sociétés ou de collections privées qui ne doivent, à mon avis, pas réclamer d'argent en échange. De plus, franchement, qui lit les crédits photos au début ou à la fin d'un livre ? très peu de personnes, donc la notoriété via ce principe est quasi nulle selon moi. Conclusion, autant me faire payer dans ce cas.

<h2>Mais combien ? Bonne question !</h2>

<p>Après quelques recherches je suis tombé sur les barèmes de l'AGAPP et de l'UPP. Je me suis défini un prix en fonction de ces barèmes et je l'ai proposé à l'éditeur, ce dernier a refusé car pas assez de budget. Sincèrement ce n'est pas si grave si ces deux photos ne sont pas pas publiées, je préfère les garder au lieu de les voir publier sachant que des gens se feront en partie de l'argent dessus. Fin de l'histoire.</p>
