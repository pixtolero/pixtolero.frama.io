---
layout:     post
title:      "Ma première véritable tentative dans le HDR"
subtitle:   ""


date:       2015-08-30
author:     "owner_name"
header-img: "img/postcover/divers.jpg"
tags: [HDR]
categories: [nouvelles,divers]
comments: true
---

<p>À Salzbourg, j'avais pris des clichés dans la Cathédrale Saint-Rupert, j'avais été obligé de sortir le trépied de voyage à cause de la faible luminosité. J'en ai donc profité pour vraiment me mettre à la photo HDR (High Dynamic Range), une image avec une forte dynamique. Avant je faisait du faux HDR à partir d'une photo avec un traitement bien particulier.</p>

<p>La technique pour réaliser une photo HDR est relativement simple, il faut prendre la même photo mais à des expositions différentes. Actuellement mon Canon EOS 550D permet de&nbsp;réaliser&nbsp;ce "bracketting" qu'avec 3 photos. En installant le micro-logiciel <a href="http://www.magiclantern.fm/">Magic Lantern</a> sur une carte SDHC, je peux élargir les possibilités de l'appareil dans ce domaine.</p>

<p>Ainsi, j'ai pris 5 clichés (-2, -1, 0,&nbsp;+1, +2 EV). Ensuite je suis passé par logiciel <a href="http://qtpfsgui.sourceforge.net/">LuminanceHDR</a>, qui regroupe les cinq photos. Après quelques réglages, j'en ai enfin pu en tirer un fichier tiff 16bits que j'ai remanié avec Darktable pour la touche finale et enfin produire un fichier jpeg.</p>

<p>Voici le résultat:</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/21504277315/in/album-72157632498787046/" title="Saint-Rupert Cathedral HDR"><img src="https://c4.staticflickr.com/1/586/21504277315_ff9652fd54_c.jpg" width="800" height="535" alt="Saint-Rupert Cathedral HDR"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/20883159993/in/album-72157632498787046/" title="Saint-Rupert Cathedral HDR"><img src="https://c2.staticflickr.com/1/658/20883159993_5b6beb65cb_c.jpg" width="800" height="563" alt="Saint-Rupert Cathedral HDR"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


<p>Pour en savoir plus sur le HDR, je vous invite à consulter ce lien:
<a href="http://www.posepartage.fr/apprendre/dossiers-techniques/photo-hdr.html">http://www.posepartage.fr/apprendre/dossiers-techniques/photo-hdr.html</a>
