---
layout:     post
title:      "Mon premier L"
subtitle:   ""


date:       2013-07-18
author:     "owner_name"
header-img: "img/postcover/divers.jpg"
tags: [nouvelles]
categories: [nouvelles]
comments: true
---

<p>Après plusieurs recherches, j'ai pris l'EF 17-40 F4 L USM en occasion chez <a href="http://www.objectif-bastille.com/epages/ObjectifBastille.sf/fr_FR/?ObjectPath=/Shops/objectifbastille/Categories/Occasion_photo/Occasion_canon/Occasion_Objectif">Objectif Bastille</a>, très bonne boutique à Paris avec du matériel en occasion à des prix raisonnables (à la côte normale de <a href="http://www.chassimages.com/index.php?page=cote">Chasseurs d'Images</a>). Comme c'était ma première commande chez cette boutique j'étais un peu suspicieux mais pas longtemps, ma commande est arrivée dans ma boîte le jour même et j'ai pu constater que l'objectif était en très bon état avec les bouchons, le pare-soleil et le sac.</p>

<p>Les focales fixes Canon et autres que j'ai trouvées étaient soit peu performantes ou soit assez chers. J'ai également pensé au Tamron 17-50 qui a une ouverture de 2.8 et qui se trouve à un bon prix mais quand on entend sur <a href="http://www.youtube.com/watch?v=8Q1AKNcqtw4">Youtube</a> le boucan généré par l'Autofocus, cela donne pas envie de l'acheter. Il y a bien sûr le 17-55 2.8 qui est relativement cher et aussi le 15-85 mais qui avec une ouverture non constante. Au final, le 17-40 est un bon choix pour un zoom standard sur APS-C et dont le range ne chevauche pas mes autres objectifs.</p>


<p>Le 17-40 a l'USM (que j'ai déjà sur mon 100mm Macro), il est très appréciable d'avoir un AF rapide et silencieux. J'ai pu constater que les objectifs L sont fidèles à leur réputation, en effet, la construction est de qualité, mon 17-40 semble tropicalisé et il ne s'allonge pas quand on touche à la bague de zoom. Je n'ai pas encore eu le temps de faire beaucoup de photos avec mais la qualité semble assez satisfaisante. Le petit plus est qu'il est compatible pour les capteurs grand format, &nbsp;dans le cas où un jour j'en disposerais d'un, je n'aurais pas revendre cet objectif.</p>


<p>Bien sûr ce 17-40 a des défauts, l'ouverture est de 4 ce qui n'est pas lumineux mais a le mérite d'être constante sur tout le range. Je pense dédié cet objectif aux photos en extérieur dans un premier temps avant de m'équiper d'un flash cobra. Pour l'APS-C, le paresoleil par défaut n'est pas adapté (flare et vignettage au rendez-vous) alors je vais me procurer l'EW-83J (prévu pour l'EF-S 17-55) qui s'adapte très bien au 17-40 comme il est dit <a href="http://blog.davidburren.com/2006/11/alternate-canon-lens-hoods.html">ici</a>. Il ne dispose pas de la stabilisation mais à ce range cela ne me dérange pas tant que cela, et puis j'ai maintenant un trépied et bientôt un flash. :)</p>

<p>
<a href="http://1.bp.blogspot.com/-sLUuFHHhOHQ/UeeVYxGLChI/AAAAAAAAb8w/Gt3jghQUk4g/s1600/2013+-+1" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="https://1.bp.blogspot.com/-sLUuFHHhOHQ/UeeVYxGLChI/AAAAAAAAb8w/Gt3jghQUk4g/s320/2013+-+1" width="320" /></a>
