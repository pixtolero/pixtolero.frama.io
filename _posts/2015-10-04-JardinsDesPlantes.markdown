---
layout:     post
title:      "Macrophotographie au Jardin des Plantes"
subtitle:   ""


date:       2015-10-04
author:     "owner_name"

tags: [divers,macro]
categories: [divers,macro]
comments: true
---

<p>Donc après mon tour à la ménagerie, petite promenade au Jardin des Plantes, histoire de faire quelques macro de jolies fleurs. Bon là aussi à main levé c'est moins évident d'obtenir une bonne mise au point mais j'ai quand même réalisé quelques clichés acceptables:.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41352551885/in/album-72157695342923931/" title="Jardin des Plantes"><img src="https://farm1.staticflickr.com/905/41352551885_4178cd6571_c.jpg" width="800" height="533" alt="Jardin des Plantes"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


