---
layout:     post
title:      "Exposition Robots: des films aux jouets"
subtitle:   ""


date:       2013-08-11
author:     "owner_name"

tags: [expo,paris]
categories: [divers]
comments: true
---

<p>Lors de ma visite au Musée des Arts et Métiers, il y a avait une exposition temporaire ayant pour thème les Robots: de films aux jouets. L’exposition était rassemblée sur une seule pièce (donc peu d’espace) mais on pouvait y voir des robots grandeur nature d’un Cylon de Battlestar Galactica, de C3PO de la Guerre des étoiles, du Terminator, un R2D2 (mais sous verre), l’armure de Robocop et pour les plus vieux, le robot Robby de Planète Interdite (1956). Il y a aussi 2,3 vitrines de jouets de collection et un écran avec des bandes-annonces de blockbusters tels que Pacific Rim. Coté photo je suis assez content du portrait que j’ai fait de C3PO. Bref, une petit expo qui mérite le détour si vous êtes fan de robots et science-fiction.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/40666471085/in/album-72157692681304552/" title="Expo Robots"><img src="https://farm1.staticflickr.com/838/40666471085_dfa44cd94d_c.jpg" width="800" height="533" alt="Expo Robots"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
