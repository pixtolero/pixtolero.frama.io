---
layout:     post
title:      "Expo Orient Express"
subtitle:   ""


date:       2014-08-17
author:     "owner_name"

tags: [expo,paris]
categories: [divers]
comments: true
---

<p>En juillet j'ai été à la fameuse exposition sur l'Orient Express à l'Institut du Monde Arabe à Paris. En face de l'institut sont présentés séparément une locomotive et 4 wagons. La locomotive est en exposition libre et gratuite. La visite débute par les wagons et se poursuit à l'intérieur de l'institut dans deux salles en sous-sol. Le plus intéressant reste l'intérieur des wagons qui a été reconstitué d'une manière authentique. En fait 3 wagons sont visitables car le 4ème est un wagon restaurant qui sert réellement de restaurant durant la durée de l'expo. On ne pouvait pas prendre de photos dans le troisième, c'est bien dommage car c'était celui des cabines avec des reconstitutions vraiment sympas comme la cabine de Matahari, celle de James Bond dans Bons Baiser de Russie ou encore celle avec un simulacre d'un cadavre sous un drap en référence au film Le Crime de l'Orient Express. Dans les autres wagons, on pouvait y trouver des décors superbement agrémentés de détails comme des journaux ou livres, passeports, cendrier...le tout d'époque. Je me suis amusé à donner un style un peu vieillot aux photos pour rester dans l'ambiance et toujours grâce au logiciel Darktable. D'ailleurs vu que c'était de la photo d'intérieur j'ai utilisé mon flash Yongnuo avec diffuseur, en faisant rebondir la lumière sur le côté j'ai pu arriver à un résultat convenable tout en évitant de monter en iso.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/14940255271/in/album-72157646531027555/" title="Locomotive"><img src="https://farm6.staticflickr.com/5593/14940255271_73ff0222b5_c.jpg" width="800" height="533" alt="Locomotive"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
