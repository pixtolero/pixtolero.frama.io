---
layout:     post
title:      "Besucherpark Flughafen München"
subtitle:   ""


date:       2015-08-28
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation,munich]
categories: [aviation]
comments: true
---

<p>Près de l'aéroport de Munich j'ai aperçu une sorte de parc avec des avions en exposition statique. Après avoir enfin trouvé l'entrée, j'ai fini par comprendre que c'est le Besucherpark de l'aéroport de Munich. Pour un euro, vous passez un portique qui vous donne accès à trois appareils mythiques, un Super Constellation, un DC-3 et un Junkers 52. On peut visiter l'intérieur du Constel et du Ju 52. C'est un lieu sympathique pour les touristes et les locaux avec un parc pour enfants, un magasin avec objets aéronautiques et il y a même un restaurant nommé Tante Ju. Je n'ai pas hésité à faire quelques clichés avec le 17-40mm et le 100mm.</p>

<p>Pour encore un euro, vous passez un autre portique, qui permet d'aller sur une bute qui domine l'aéroport, ainsi on peut admirer décollages et atterrissages. Si le musée Delta d'Orly survit il devrait vraiment s'inspirer du Besucherpark.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41331030905/in/album-72157667137182337/" title="Ju-52"><img src="https://farm1.staticflickr.com/830/41331030905_f8213da74a_c.jpg" width="800" height="533" alt="Ju-52"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
