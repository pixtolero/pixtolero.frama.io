---
layout:     post
title:      "Fly In Stampe Pithiviers 2016"
subtitle:   ""


date:       2016-07-10
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation]
categories: [aviation]
comments: true
---

<p>Le weekend dernier, j'ai été au Fly In Stampe à l'aérodrome de Pithiviers. La météo était au rendez-vous et le plateau offrait quelques avions interéssants. En effet malgré que cela soit un rassemblement de stampe, les autres avions de collection étaient les bienvenus. Ainsi, en plus de la douzaine de Stampe, on pouvait compter trois Yak 52, un Bucker Jungmeister, un Leopoldoff ou encore un Bellanca Cruisemaster.</p>

<p>Ce n'est pas le meeting de la Ferté-Alais et c'est tant mieux, moins de monde (une grande majorité de passionnés) et une ambiance très chaleureuse grâce à l'aéroclub du Pithiverais. De plus, il était possible d'approcher les avions au sol de très près. Bref si vous êtes fans d'avions et êtes situé près du Loiret, n'hésitez pas y aller.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/40535174370/in/album-72157695518842781/" title="Fly In Stampe 2016"><img src="https://farm2.staticflickr.com/1723/40535174370_7441db73c1_c.jpg" width="800" height="533" alt="Fly In Stampe 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
