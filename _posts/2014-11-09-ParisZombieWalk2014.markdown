---
layout:     post
title:      "Paris Zombie Walk 2014"
subtitle:   ""


date:       2014-11-09
author:     "owner_name"

tags: [zombie,portrait,cosplay,paris]
categories: [portrait]
comments: true
---

<p>Hier j'ai assisté à ma troisième zombie walk de Paris. Comme l'année précédente le rassemblement avait lieu sur la Place de la République. La nouveauté cette fois-ci était la présence d'animations comme un concert et des danses (dont certaines enflammées) durant le rassemblement précédant la marche. Encore une fois, le nombre de photographes égalait surement celui de gens déguisés en zombie. Certains costumes faisaient preuve d'originalité comme par exemple le Joker et sa canette de Coca, les infirmières enragées ou le soldat de la première guerre mondiale. Je n'ai utilisé que le 85mm. Tout le traitement a été fait via Darktable. Voici une sélection, le reste des photos se trouve <a href="https://www.flickr.com/photos/danpilote/albums/72157649203189132">sur Flickr</a>:</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/42074852161/in/album-72157668911377618/" title="Paris Zombie Walk 2014"><img src="https://farm1.staticflickr.com/910/42074852161_68bf6a9db5_c.jpg" width="800" height="533" alt="Paris Zombie Walk 2014"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
