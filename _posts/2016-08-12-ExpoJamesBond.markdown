---
layout:     post
title:      "Expo James Bond"
subtitle:   ""


date:       2016-08-12 
author:     "owner_name"

tags: [divers,paris,expo]
categories: [divers]
comments: true
---

<p>Étant fan de l'agent 007, je me suis laissé tenté par l'exposition James Bond, actuellement à la Grande Halle de la Villette jusqu'à septembre. Si voulez faire des photos, attention le flash est interdit et certaines parties de l'expo sont sombres. Pour ma part, j'ai réussi à me débrouiller comme j'ai pu.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/28467305378/in/album-72157691458010820/" title="Exposition James Bond"><img src="https://farm2.staticflickr.com/1760/28467305378_7b6c5073c8_c.jpg" width="800" height="533" alt="Exposition James Bond"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
