---
layout:     post
title:      "Avengers S.T.A.T.I.O.N"
subtitle:   ""


date:       2016-07-09 
author:     "owner_name"
header-img: "img/postcover/cosplay.jpg"
tags: [divers,marvel,expo,paris]
categories: [divers]
comments: true
---

<p>Il y a actuellement une exposition consacré aux héros Avengers de Marvel à l'esplanade de La Défense. J'y suis allé et voici mes impressions.</p>

<p>Avant de commencer l'expo, il est possible de tester un casque de réalité virtuelle au stand de Samsung. C'est sympa mais sans plus, les graphismes ne m'ont pas convaincu, peut être que l'Occulus Rift est meilleur.</p>

<p>Durant l'expo, on parcourt différentes salles consacrée aux personnages principaux, dans l'ordre, Captain America,  Hulk, Black Widow, Thor et Iron Man.</p>

<p>Dans chaque salle on peut lire un tas d'infos qui ne sera d'aucune utilité pour les fans. Au fur et à mesure, sur un appareil mobile on peut écouter des explications et répondre à des questionnaires. J'ai fait un 100%, ce n'est pas vraiment utile sauf si à la fin à la boutique vous voulez votre carte d'avancer...qui est payante. Et oui la fin de l'expo est très axée sur le merchandising.</p>

<p>Néanmoins durant l'expo, on peut retrouver les costumes et accessoires des personnages, les armures d'Iron Man comme l'Hulkbuster en 2 morceaux car le plafond est un peu juste.</p>

<p>Au final, je ne suis pas mécontent d'avoir profité d'un prix réduit en juin, car le prix normal est un peu chère pour cette expo sans compter ce qu'il faut débourser pour repartir avec un souvenir.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/27469729747/in/album-72157694085855072/" title="Avengers S.T.A.T.I.O.N"><img src="https://farm2.staticflickr.com/1752/27469729747_76da91b0fc_c.jpg" width="800" height="533" alt="Avengers S.T.A.T.I.O.N"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
