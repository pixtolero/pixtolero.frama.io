---
layout:     post
title:      "Canon EF 85mm F1.8 USM"
subtitle:   ""


date:       2013-11-26
author:     "owner_name"
header-img: "img/postcover/divers.jpg"
tags: [nouvelles]
categories: [nouvelles]
comments: true
---

<p>Voici mon nouvel objectif, le Canon EF 85mm à ouverture 1/1.8 et à motorisation USM. Je l’ai eu à un prix raisonnable en occasion sur le site d’Objectif Bastille. L’objectif est en très bon état et vient avec son pare-soleil officiel Canon, l’ET-65 III (et qui vas m’économiser une trentaine d’euros) qui ne se visse pas mais se clipse. La taille des filtres est la même que mon 55-250 (58mm) donc je vais pouvoir réutiliser les filtres que j’avais déjà achetés pour ce dernier. C’est une focale fixe de qualité qui donne environ 135mm sur le capteur APS-C de mon 550D. L’ouverture 1.8 me permettra de réaliser de jolis bokeh et la motorisation ultra-sonique USM je ne peux plus vraiment m’en passer depuis mon 100mm Macro et mon 17-40. Certes 85mm est dans le range de mon 55-250mm mais je souhaites me séparer de ce dernier car pas lumineux et je préfère de plus en plus les focales fixes. Je dédie cet objo en particulier pour le portrait là où mon 100mm Macro ne peut pas toujours satisfaire avec un piqué un peu trop élevé sur certains visages. J’essayerai de réaliser prochainement des photos avec la bête et l’apprivoiser rapidement.</p>


<a href="http://2.bp.blogspot.com/-RjmgtYV-vBI/UpR7QV6RS-I/AAAAAAAAgTw/8vBrdJUCaME/s1600/2013+-+1" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-RjmgtYV-vBI/UpR7QV6RS-I/AAAAAAAAgTw/8vBrdJUCaME/s640/2013+-+1" /></a></span>

<a href="http://2.bp.blogspot.com/-ZfZpO7X3acI/UpR7QQfoSSI/AAAAAAAAgT4/mWqQNn1kIMM/s1600/2013+-+2" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-ZfZpO7X3acI/UpR7QQfoSSI/AAAAAAAAgT4/mWqQNn1kIMM/s640/2013+-+2" /></a>