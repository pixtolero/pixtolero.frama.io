---
layout:     post
title:      "Pillow Fight Day 2014 à Paris"
subtitle:   ""


date:       2014-06-04
author:     "owner_name"

tags: [paris]
categories: [divers]
comments: true
---

<p>Ce samedi, en début d'après-midi, j'ai été, un peu par curiosité, à la Pillow Fight Day de Paris qui avait lieu sur la Place de la République. Cet événement est un flash mob où les participants viennent avec leur polochon pour une bataille géante. On pouvait aussi y venir déguisé. Coté photographes, j'étais loin d'être le seul, l'ambiance m'a rappelé un peu la Zombie Walk de l'an dernier qui s'était déroulé au même endroit. Une fois le coup de sifflet lancé, les participants se sont tous mis à se taper dessus avec leurs polochons dans la bonne humeur. Bref, cela fût assez amusant à regarder, voici une sélection de photos:</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/26644217907/in/album-72157667894552858/" title="Pillow Fight Day 2014 (Paris)"><img src="https://farm1.staticflickr.com/937/26644217907_fc7cf48620_c.jpg" width="533" height="800" alt="Pillow Fight Day 2014 (Paris)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
