---
layout:     post
title:      "Paris Comics Expo 2014"
subtitle:   ""


date:       2014-11-23 
author:     "owner_name"
header-img: "img/postcover/cosplay.jpg"
tags: [portrait,cosplay]
categories: [portrait,cosplay]
comments: true
---

<p>Samedi dernier je me suis rendu à la Paris Comics Expo. J'avais été à la première édition en 2012 et cette année je dois avouer que la convention a pris vraiment de l'ampleur. Le nombre de stand a augmenté mais aussi le nombre de visiteurs. Pour le concours cosplay, avec la configuration de la scène on y voyait pas grand chose. J'ai quand même fait des photos en extérieur mais je comme je l'avais déjà dit, l'espace Champerret a des conditions très limitées pour y faire des photos sympas. Pas sûr que j'y retourne l'an prochain ou alors sans y entrer, dehors il y a suffisamment de quoi faire.</p>

<p>Bon même je ne suis pas satisfait du résultat, voici quelques photos:</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/42031737202/in/album-72157696762382135/" title="The Walking Dead"><img src="https://farm1.staticflickr.com/958/42031737202_8e3a114fd7_c.jpg" width="800" height="533" alt="The Walking Dead"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
