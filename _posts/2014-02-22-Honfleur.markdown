---
layout:     post
title:      "Honfleur"
subtitle:   ""


date:       2014-02-22
author:     "owner_name"

tags: [paysage]
categories: [paysage]
comments: true
---

<p>Voici une série complète de photos prises durant un passage à Honfleur lors d'une après-midi ensoleillée de ce mois de février. Une charmante petite ville portuaire de Normandie, lieu idéal pour y faire des photos de paysage. Honfleur ce n'est pas uniquement le bassin du centre-ville, les petites ruelles et les maisons normandes sont également de bons sujets de photos. J'ai fais toutes les photos avec le 17-40 F4L et avec ensuite du traitement via Lightroom. J'ai donné à l'une des photos un style rétro qui colle très bien je trouve. Si vous n'avez jamais visité Honfleur, j'espère que mes photos vous donneront envie de vous y rendre :-)</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41533104701/in/album-72157695063104114/" title="Honfleur"><img src="https://farm1.staticflickr.com/885/41533104701_8035ac8294_c.jpg" width="800" height="533" alt="Honfleur"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
