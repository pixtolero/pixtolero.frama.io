---
layout:     post
title:      "Salon du Bourget 2015"
subtitle:   ""
image:

date:       2015-06-20
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation,bourget]
categories: [aviation]
comments: true
---

<p>Cette semaine se tient le 51ème Salon International de l'Aéronautique et de l'Espace au Bourget. J'y suis allé durant la journée du mercredi (17 juin). En fait j'ai pu obtenir une invitation pour l'une des journées réservés aux professionnels (plus calme que le week end ouvert au public). Après mon avis est un peu partagé sur ce salon, je rejoins un peu <a href="http://www.avionslegendaires.net/2015/06/actu/pourquoi-je-nirais-pas-au-bourget-cette-annee/" target="_blank">l'opinion du rédacteur d'AvionsLegendaires</a>.&nbsp;La non-présence des avions militaires russes se faisait ressentir. L'armée américaines était présente avec quand même du déjà vu (F-16, F-15, Chinook...) et en plus le tout cloué au sol. Néanmoins j'ai pu voir le dernier né d'Airbus, l'A350 et des appareils un peu exotiques comme le Scorpion de Textron et le JF-17 sino-pakistanais. Les plus anciens doivent regretter l'époque où l'on pouvait s'attendre à des choses uniques comme ce fut le cas à une époque avec les avions furtifs de l'US Air Force ou le Tupolev 144 (Concorde russe). On a parfois l'impression que ce salon se résume juste à une guerre de commandes d'avions entre Airbus et Boeing.</p>

<p>J'ai profité de la matinée pour faire des photos des appareils en statique, pas facile avec les commerciaux au tour et les barrières. J'ai rapidement fait le tour et j'ai profité du temps qu'il me restait avant les démonstrations aériennes pour aller voir les nouveautés du Musée de l'Air et de l'Espace dont je consacrerai un ou deux billets prochainement.</p>

<p>Vers 12h00, direction le parking autos. Comme il y a deux 2 ans, je trouvais que c'étais le meilleur endroit pour prendre les aéronefs en vol. Dans un coin en face du seuil de piste 03, j'ai trouvé des spotters équipés de téléobjectifs, j'ai trouvé judicieux de rester au même endroit (et ce fût une bonne idée). La journée était ensoleillée et la lumière était photographiquement dure mais j'ai pour réaliser quelques clichés convenables avec le 200mm+1.4x.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41410941384/in/album-72157693714868562/" title="Le Bourget 2015"><img src="https://farm1.staticflickr.com/950/41410941384_6ff301ba5a_c.jpg" width="800" height="533" alt="Le Bourget 2015"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
