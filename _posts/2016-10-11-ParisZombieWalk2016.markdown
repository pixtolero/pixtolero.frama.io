---
layout:     post
title:      "Paris Zombie Walk 2016"
subtitle:   ""


date:       2016-10-11
author:     "owner_name"

tags: [zombie,portrait,cosplay,paris]
categories: [portrait]
comments: true
---

<p>Samedi dernier se tenait l'édition 2016 de la Paris Zombie Walk et une fois encore j'en ai profité pour faire quelques portraits. D'habitude je prenais le 85mm et le 17-40mm mais j'ai décidé cette année de ne me munir que de mon nouveau 35mm qui ouvre jusqu'à F2. Je suis assez content du résultat. Une fois encore bravo aux zombies pour leurs maquillages, leurs déguisements et leurs prestations. </p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41637394654/in/album-72157696672662314/" title="Paris Zombie Walk 2016"><img src="https://farm2.staticflickr.com/1723/41637394654_4b60518c99_c.jpg" width="533" height="800" alt="Paris Zombie Walk 2016"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
