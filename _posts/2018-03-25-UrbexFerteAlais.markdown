---
layout:     post
title:      "Tentative d'Urbex à La Ferté"
date:       2018-03-25
author:     "owner_name"
header-img: "img/postcover/aviation.jpg"
tags: [aviation]
categories: [aviation]
comments: true
---

<p>En parcourant le terrain de La Ferté, je suis tombé sur l'exemplaire de TEGAS, un prototype d'avion destiné à emporter dans sa soute cargo une toute petite voiture. L'engin est abandonné à l'écart dans un bois et est bien amoché. Du coup, j'ai pris des photos avec le téléphone portable (Xiaomi Mi 5). En traitant les photos avec darktable, j'ai tenté du Noir et Blanc et aussi un truc un peu post-apocalyptique façon Urbex. Le résultat de l'essai n'est pas si mal:</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/40112535955/in/dateposted-public/" title="TEGAS"><img src="https://farm1.staticflickr.com/803/40112535955_8c63a96ceb_z.jpg" width="640" height="480" alt="TEGAS"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/40963664992/in/dateposted-public/" title="TEGAS"><img src="https://farm1.staticflickr.com/820/40963664992_575c1a6d9b_z.jpg" width="640" height="480" alt="TEGAS"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/41005619841/in/dateposted-public/" title="TEGAS"><img src="https://farm1.staticflickr.com/811/41005619841_4b8eacf520_z.jpg" width="640" height="479" alt="TEGAS"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<p>Au passage, le DC-3 a perdu sa dérive et ses bouts d'ailes, son avenir semble incertain...</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/danpilote/40112573595/in/dateposted-public/" title="DC-3"><img src="https://farm5.staticflickr.com/4777/40112573595_2693a62921_z.jpg" width="640" height="479" alt="DC-3"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>






