---
layout:     post
title:      "De Lightroom à Darktable"
subtitle:   ""


date:       2014-08-05
author:     "owner_name"
header-img: "img/postcover/divers.jpg"
tags: [lightroom,darktable]
categories: [nouvelles]
comments: true
---


<p>En ce moment je tente de délaisser le logiciel de traitement photo Lightroom d'Adobe au profit de <a href="http://www.darktable.org/" target="_blank">Darktable</a>, un logiciel libre disponible sur Linux (que j'utilise au quotidien sur mon ordinateur avec <a href="http://elementaryos.org/" target="_blank">elementary OS</a>) et Mac.</p>

<p><a href="http://www.darktable.org/" target="_blank">Darktable</a> est un logiciel puissant et munis de plein de fonctionnalités qui ne sont pas toutes évidentes à prendre en main au début. Heureusement il y a la chaîne <a href="https://www.youtube.com/user/carafife/videos" target="_blank">Carafife</a> sur Youtube qui propose un tas de tutoriels vidéos.</p>

<p>De plus je projette de monter un second PC dans un boitier de format mini-itx booster avec un bon i5 et 8go de ram, ainsi, je pense gagner en productivité et en temps pour le traitement photo. Stay tuned !</p>


<a href="http://2.bp.blogspot.com/-uve2Fjz_a2o/U-Cz4vsTq5I/AAAAAAAAkXw/3t_wPFdoako/s1600/Capture+du+2014-08-03+19:50:11.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-uve2Fjz_a2o/U-Cz4vsTq5I/AAAAAAAAkXw/3t_wPFdoako/s640/Capture+du+2014-08-03+19:50:11.png" /></a>
