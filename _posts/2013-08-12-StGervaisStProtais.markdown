---
layout:     post
title:      "L’église Saint-Gervais-Saint-Protais à Paris"
subtitle:   ""


date:       2013-08-12
author:     "owner_name"

tags: [paris]
categories: [divers]
comments: true
---

<p>Entre l’Hôtel de Ville de Paris et le Mémorial de la Shoah, se trouve l’église Saint-Gervais-Saint-Protais. C’est un peu par hasard en me balladant que j’ai aperçu la façade avant. J’ai donc décider d’y aller faire un tour. L’endroit est un peu caché dans le 4ème arrondissement et n’a pas l’air d’être la proie des touristes comme c’est le cas avec Notre-Dame. Les églises sont très photogénique grâce à leur architecture. Voici quelques photos, vous verrez qu’à un moment, j’ai même sortie le 8mm fisheye pour m’amuser un peu :)</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/9483454271/in/album-72157635024970322/" title="2013-08-08_16-04-44EgliseStGervais"><img src="https://farm4.staticflickr.com/3667/9483454271_6c79d01e89_c.jpg" width="800" height="533" alt="2013-08-08_16-04-44EgliseStGervais"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
