---
layout:     post
title:      "Danmarks Tekniske Museum"
subtitle:   ""


date:       2013-09-07
author:     "owner_name"

tags: [aviation]
categories: [aviation]
comments: true
---

<p>Au Danemark, j'ai eu la chance d'aller visiter le Tekniske Museum situé près d'Helsingor, à 40 minutes en voiture au nord de Copenhague. A l’extérieur, un T-33 et une locomotive à vapeur sont fièrement exposés. Le musée est en fait un énorme hangar qui cache une très riche collection de machines à vapeur, de dynamos, de voitures, de moto, de vélos, d'engins volants,d' appareils électroniques ou d'électroménager, outils scientifiques... De plus, le musée est très interactif, par exemple, en montant dans le DC-3 le son du démarrage des moteurs se met en marche et le plancher se met à trembler. Un exemplaire de l'avion de ligne de construction française, Caravelle, y est exposé et on peut même le visiter. Des bénévoles du musée se sont lancés dans la construction d'un hydravion FF49C dont vous pouvez voir l'avancement sur l'une des photos. À noter également l'épave repêchée d'un hydravion allemand, Blohm Und Voss, de la seconde guerre mondiale. Il y avait une exposition temporaire sur une artiste et pilote danoise qui a relié Copenhague à Kaboul sur Piper Colt, bel exploit.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41467114901/in/album-72157694941046694/" title="Danmarks Tekniske Museum"><img src="https://farm1.staticflickr.com/783/41467114901_bb3976342e_c.jpg" width="533" height="800" alt="Danmarks Tekniske Museum"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>s://www.flickr.com/photos/danpilote/9723752602/in/album-72157635479927500/" title="2013-08-25_11-23-54TekniskMuseum"><img src="https://c3.staticflickr.com/3/2843/9723752602_59b328698c_c.jpg" width="800" height="534" alt="2013-08-25_11-23-54TekniskMuseum"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

