---
layout:     post
title:      "STAR WARS Identities"
subtitle:   ""


date:       2014-05-04
author:     "owner_name"

tags: [divers,expo,paris]
categories: [divers]
comments: true
---

<p>En ce 4 mai, journée mondiale Star Wars (May the fourth be with you), voici des photos prises lors de ma visite de l'exposition STAR WARS Identities à la Cité du Cinéma à Saint-Denis. Bien que le billet soit un peu cher, environ 22€, je pense que le fan de Star Wars ne sera pas mécontent. L'expo, en plus de partager une large collection de costumes, d'objets, de planches de dessins...etc issus de la saga, elle est surtout interactive. Le but est de créer votre identité Star Wars, selon les choix que vous aurez pris tout le long du parcours. Muni d'un bracelet, vous aurez accès à des bornes où vous devrez répondre à des questions qui définissent votre caractères, vos origines...etc. Bien sûr, à la fin, la dernière borne est le choix entre le coté lumineux et obscure de la Force. Après, j'ai pu faire apparaître mon avatar sur un mur. Cet expo est vraiment sympa et je la recommande vivement. Coté photo, c'était un peu galère car le salles sont obscures et je n'aime pas trop utiliser le flash alors j'ai monté les ISO au risque d'avoir du grain, j'ai quand même obtenu des choses potables:</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/40799975434/in/album-72157692602754962/" title="STARWARS Identities"><img src="https://farm1.staticflickr.com/805/40799975434_59d3c3ae12_c.jpg" width="800" height="533" alt="STARWARS Identities"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
