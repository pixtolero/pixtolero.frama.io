---
layout:     post
title:      "Paris Zombie Walk 2015"
subtitle:   ""


date:       2015-10-05
author:     "owner_name"

tags: [zombie,portrait,cosplay,paris]
categories: [portrait]
comments: true
---

<p>Ce samedi 3 octobre se tenait la Paris Zombie Walk. Tout comme pour l'édition 2014, le rassemblement avait lieu Place de la République. C'était la quatrième fois que j'y allais et l'ambiance y est toujours aussi sympathique étant par les zombies que les animations autours (concert, danses...)</p>

</p>Je n'ai fait pas la marche mais juste le rassemblement de 13 à 15h. La journée était ensoleillée et donc propice à la photo. J'ai retrouvé les éternels classiques: soldat zombie, infirmières zombies, le couple de mariés zombie...etc. D'autres ont été plus originaux comme le groupe de fermiers zombies avec une banderole "Les Zombies sont dans le Pré".</p>

<p>J'ai fait facilement passé plus d'une heure à shooter avec le 85mm pour faire des portraits avec courte profondeur de champs. J'ai changé par la suite pour le 17-40 pour obtenir des plans plus larges. Parfois la météo pouvait être un peu bâtarde (contre-jour...) et l'appareil avait tendance à sur ou sous-exposer.</p>

<p>Bien que d'apparence hostile, les zombies étaient tous sympathiquement ravis de poser :-)</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41556070174/in/album-72157696500413124/" title="Paris Zombie Walk 2015"><img src="https://farm1.staticflickr.com/873/41556070174_0aff099a9a_c.jpg" width="800" height="533" alt="Paris Zombie Walk 2015"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
