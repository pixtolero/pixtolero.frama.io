---
layout:     post
title:      "Salon du Fantastique 2013"
subtitle:   ""


date:       2013-11-03 
author:     "owner_name"
header-img: "img/postcover/cosplay.jpg"
tags: [cosplay,portrait]
categories: [cosplay,portrait]
comments: true
---

<p>Ce dimanche j’ai été à la deuxième édition du Salon du Fantastique (l’entrée était gratuite si on réservait son billet à l’avance sur internet) à l’Espace Champerret. Bonne organisation générale, pas mal de stands divers et variés. Je voulais y aller cette journée en particulier pour assister au concours de déguisements. L’éclairage dans ce lieu n’est vraiment pas au top et j’étais pas relativement bien placé pour prendre des photos. Après sélection des photos, je n’en ai que six que je qualifie de « potables ». Vu la distance où j’étais placé, j’ai opté pour le 100mm qui, malgré son ouverture, a peiné. Le concours en lui même s’est bien déroulé, le papy « davy crockett » était assez drôle. Le seul bémol est la sonorisation, mes tympans ont très soufferts par moments. Depuis le festival Harajuku au parc de Bercy, je ne prend plus seulement attention au cosplayer en lui-même mais aussi à l’environnement qui l’entoure et à l’espace Champerret…c’est pas vraiment l’idéal. Cela explique aussi mon tri draconien des photos que j’ai prises.</p>

<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/41407322702/in/album-72157694909966604/" title="Salon du Fantastique 2013"><img src="https://farm1.staticflickr.com/822/41407322702_1773d30519_c.jpg" width="800" height="533" alt="Salon du Fantastique 2013"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
