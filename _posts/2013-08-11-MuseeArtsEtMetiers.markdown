---
layout:     post
title:      "Musée des Arts et Métiers"
subtitle:   ""


date:       2013-08-11
author:     "owner_name"

tags: [paris]
categories: [divers,aviation]
comments: true
---

<p>Voici quelques photos prise au Musée des Arts et Métiers à Paris durant la semaine. C’était la première fois que je visitais ce musée qui est assez grand et qui possède une collection diverse et variée. Je suis arrivé durant la démonstration du pendule de Foucault dans la chapelle qui jouxte le bâtiment. Cette chapelle n’est pas super bien éclairée et c’était un peu compliqué avec le 17-40 (vive les photos floutées..), j’aurais pu sortir le 50mm 1.8 mais je pense pas que j’aurais eu assez de recul pour prendre les avions et voitures exposées en entier. Dans le reste de la collection, il y avait beaucoup de maquettes mais sous verre, ce qui n’aide pas avec les reflets… La présence d’équipement télé, photo, radio…etc ancien donne un goût de nostalgie, exemple avec les vieux téléviseurs. Il y avait une exposition sur les Robots mais je ferais un billet complet dessus bientôt. Il y avait également une expo sur le dessinateur Enki Bilal mais les photos étaient strictement interdites. L’Éole de Clément Ader est accroché au-dessus un escalier un peu à l’écart, à mon humble avis, ce n’est pas le meilleur endroit pour exposer l’engin, sensé être le premier avion du monde, à sa juste valeur. Cela dit, ce musée reste néanmoins intéressant et sûrement incontournable.</p>


<a data-flickr-embed="true" data-footer="true" data-context="true"  href="https://www.flickr.com/photos/danpilote/26593587317/in/album-72157665775419007/" title="Musée des Arts et Métiers"><img src="https://farm1.staticflickr.com/807/26593587317_3f8162bd95_c.jpg" width="800" height="544" alt="Musée des Arts et Métiers"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
