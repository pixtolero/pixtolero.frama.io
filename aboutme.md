---
layout: page
title: À Propos
subtitle: Pilote, passionné d'aéronautique et de photographie.
---

<i class="fa fa-camera fa-3x" aria-hidden="true"><b>Matériel</b></i>
<ul>
<li>Canon EOS 550D&nbsp;</li>
<li>Grip Phottix BG-550D&nbsp;</li>
<li>Samyang 8mm Fish-Eye <i>(paysage, architecture)</i></li>
<li>Canon EF 17-40mm F4L USM&nbsp;&nbsp;<i>(trans-standard, reportage)</i></li>
<li>Canon EF 35mm F2 IS USM <i>(portrait)</i></li>
<li><strike>Sigma 18-200 DC</strike><i> &nbsp;(revendu)</i></li>
<li><strike>Canon EF 50mm F1.8 II&nbsp;</strike><i>&nbsp;&nbsp;(revendu)</i></li>
<li>Canon EF 85mm F1.8 USM&nbsp;&nbsp;<i>(portrait)</i></li>
<li>Canon EF 100mm F2.8 USM Macro&nbsp;&nbsp;<i>(portrait, macro, animalier)</i></li>
<li><strike>Canon EF-S 55-250mm IS II&nbsp;</strike><i>&nbsp;&nbsp;(revendu)</i></li>
<li>Canon EF 200mm F2.8L USM II&nbsp;&nbsp;<i>(avions)</i></li>
<li>Canon converter 1.4x III&nbsp;</li>
<li>Flash Yongnuo YN-568EX&nbsp;</li>
<li>Trépied Vanguard Alta Pro 263AT&nbsp;</li>
<li>Rotule Vanguard SBH-100&nbsp;</li>
<li>Monopode Manfrotto MM394&nbsp;</li>
<li>Trépied de voyage AmazonBasics&nbsp;</li>
</ul>
&nbsp;
<i class="fa fa-gears fa-3x" aria-hidden="true"><b>Logiciel</b></i>
<ul>
<li><a href="https://darktable.fr/">Darktable</a>&nbsp;</li>
<li><a href="https://gimp.org/">GIMP</a>&nbsp;</li>
</ul>
&nbsp;
<i class="fa fa-desktop fa-3x" aria-hidden="true"><b>Système d'exploitation</b></i>
<ul>
<li><a href="https://solus-project.com/">Solus</a>&nbsp;</li>
</ul>
&nbsp;
<i class="fa fa-file-text-o fa-3x" aria-hidden="true"><b>Travaux</b></i>
<ul>
<li><a href="http://airways-magazine.com/">Magazine AIRWAYS</a> (couverture, photos pour articles, rubrique coin du spotter)&nbsp;</li>
<img src="../img/airways_travaux.png">
</ul>
&nbsp;
<i class="fa fa-creative-commons fa-3x" aria-hidden="true"><b>Droits</b></i>
<br>
<b>Mes photos sont sous licence Creative Commons NC BY SA 3.0. <br /><br />Paternité + Pas d’Utilisation Commerciale + Partage dans les mêmes conditions (BY NC SA): Le titulaire des droits autorise l’exploitation de l’œuvre originale à des fins non commerciales, ainsi que la création d’œuvres dérivées, à condition qu’elles soient distribuées sous une licence identique à celle qui régit l’œuvre originale. Si vous désirez réutiliser mes photos à des fins commerciales, veuillez me contacter au préalable.</b>
<br>
<img src="../img/cc-by-nc-sa.png">
<br>
<br>
&nbsp;
<i class="fa fa-link fa-3x" aria-hidden="true"><b>Quelques liens utiles</b></i>
<h3>Photographie</h3>
<li><a href="http://www.darktable.org/">Darktable</a>&nbsp;</li>
<li><a href="http://darktable-fr.tuxfamily.org/">Darktable-fr</a>&nbsp;</li>
<li><a href="https://www.youtube.com/user/carafife">Chaine Youtube Carafife</a>&nbsp;</li>
<li><a href="https://pixls.us/">Pixls.US</a>&nbsp;</li>
<li><a href="http://fr.pixelpeeper.com/">Pixel-Peeper</a>&nbsp;</li>
<li><a href="http://www.posepartage.fr/">Pose-Partage</a>&nbsp;</li>
<li><a href="http://fotoloco.fr/">Fotoloco</a>&nbsp;</li>
<li><a href="http://dtstyle.net/">Dtstyle.net</a>&nbsp;</li>
<li><a href="http://blog.darth.ch/">Darth's Blog</a>&nbsp;</li>
<li><a href="http://ouiouiphoto.fr/Wp/">OuiOuiphoto</a>&nbsp;</li>
<li><a href="http://shoubephotoblog.blogspot.fr/">Shoube</a>&nbsp;</li>
<li><a href="http://sendman.fr/">Sendman</a><br />
&nbsp;
<h3>Aviation</h3>
<li><a href="http://www.passionpourlaviation.fr/">PassionDesAvions</a>&nbsp;</li>
<li><a href="http://www.avionslegendaires.net/">AvionsLegendaires</a>&nbsp;</li>
<li><a href="http://www.lecharpeblanche.fr/">L'Echarpe Blanche</a>&nbsp;</li>
<li><a href="http://www.pyperpote.tonsite.biz/listinmae/">List'In MAE (pyperpote)</a>&nbsp;</li>
&nbsp;
